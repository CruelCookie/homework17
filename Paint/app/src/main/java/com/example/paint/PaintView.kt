package com.example.paint

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.util.DisplayMetrics
import android.view.MotionEvent
import android.view.View
import kotlin.random.Random

class PaintView(
    context: Context,
    attrs: AttributeSet? = null
): View(context, attrs){
    private var mx = 0f
    private var my = 0f
    private var index = 0
    private var size = 100f
    private var radius = 100f
    private var isRect = false
    private var isRandom = true
    private var isRed = true
    private val rectangles = ArrayList<Rectangle>()
    private val circles = ArrayList<Circle>()

    private val backgroundColor = Color.WHITE
    private val defColorRed = Color.RED
    private val defColorGreen = Color.GREEN
    private lateinit var bitmap: Bitmap
    private lateinit var mCanvas: Canvas
    private val bitmapPainter = Paint(Paint.DITHER_FLAG)

    private val painter = Paint().apply{
        color = Color.GREEN
        style = Paint.Style.STROKE
        isAntiAlias = true
        strokeWidth = 30f
    }

    fun init(metrics: DisplayMetrics){
        bitmap = Bitmap.createBitmap(metrics.widthPixels, metrics.heightPixels, Bitmap.Config.ARGB_8888)
        mCanvas = Canvas(bitmap)
    }
    fun square(){ isRect = true}
    fun circle(){ isRect = false}
    fun randomColour(){
        isRandom = true
        isRed = false
    }
    fun greenColour(){
        isRandom = false
        isRed = false
        painter.color = defColorGreen
    }
    fun redColour(){
        isRandom = false
        isRed = true
        painter.color = defColorRed
    }
    fun clear(){
        rectangles.clear()
        circles.clear()
        invalidate()
    }


    override fun onDraw(canvas: Canvas?) {
        canvas?.save()
        mCanvas.drawColor(backgroundColor)
        for(rect in rectangles){
            painter.color = rect.color
            mCanvas.drawRect(rect.rect, painter)
        }
        for(circle in circles){
            painter.color = circle.color
            mCanvas.drawCircle(circle.x, circle.y, circle.radius, painter)
        }
        if(!isRandom and !isRed){
            painter.color = defColorGreen
        } else if(!isRandom and isRed){
            painter.color = defColorRed
        }
        canvas?.drawBitmap(bitmap, 0f, 0f, bitmapPainter)
        canvas?.restore()
    }

    private fun paintFigure(x: Float?, y: Float?){
        var flagCircle = true
        var flagRectangle = true
        for(i in circles){
            if ((x!! in (i.x-i.radius)..(i.x+i.radius)) and (y!! in (i.y-i.radius)..(i.y+i.radius))){
                flagCircle = false
                break
            }
        }
        for(i in rectangles){
            if ((x!! in (i.rect.left..i.rect.right)) and (y!! in (i.rect.bottom..i.rect.top))){
                flagRectangle = false
                break
            }
        }
        if (isRandom) painter.color = Color.argb(
            255,
            Random.nextInt(0, 256),
            Random.nextInt(0, 256),
            Random.nextInt(0, 256)
        )
        if (isRect and flagRectangle) {
            val rect = RectF(x!! - size, y!! + size, x + size, y - size)
            rectangles.add(Rectangle(rect, size, painter.color))
        } else if(!isRect and flagCircle){
            circles.add(Circle(x!!, y!!, radius, painter.color))
        }
    }
    private fun moveFigure(x: Float?, y: Float?){
        if(!isRect){
            for (i in circles.indices){
                if ((index == -1) and (x!! in (circles[i].x-circles[i].radius)..(circles[i].x+circles[i].radius)) and (y!! in (circles[i].y-circles[i].radius)..(circles[i].y+circles[i].radius))){
                    index = i
                }
            }
            if(index!=-1){
                circles[index].x = x!!
                circles[index].y = y!!
            }
        } else{
            for (i in rectangles.indices){
                if ((index == -1) and (x!! in (rectangles[i].rect.left..rectangles[i].rect.right)) and (y!! in (rectangles[i].rect.bottom..rectangles[i].rect.top))){
                    index = i
                }
            }
            if(index!=-1){
                rectangles[index].rect.top = y!!+rectangles[index].size
                rectangles[index].rect.bottom = y-rectangles[index].size
                rectangles[index].rect.left = x!!-rectangles[index].size
                rectangles[index].rect.right = x+rectangles[index].size
            }
        }
    }
    override fun onTouchEvent(event: MotionEvent?): Boolean {
        val x = event?.x
        val y = event?.y
        when(event?.action){
            MotionEvent.ACTION_DOWN -> {
                paintFigure(x, y)
                invalidate()
            }
            MotionEvent.ACTION_MOVE -> {
                moveFigure(x,y)
                invalidate()
            }
            MotionEvent.ACTION_UP -> {
                index = -1
                invalidate()
            }
        }
        return true
    }
}