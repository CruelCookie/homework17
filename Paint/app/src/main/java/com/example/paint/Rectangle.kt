package com.example.paint

import android.graphics.RectF

data class Rectangle(
    var rect: RectF,
    var size: Float,
    var color: Int
)
