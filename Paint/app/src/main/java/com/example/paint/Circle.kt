package com.example.paint

data class Circle(
    var x: Float,
    var y: Float,
    var radius: Float,
    var color: Int
)
